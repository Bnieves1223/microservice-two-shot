from django.urls import path
from .views import api_hats, api_show_hat


urlpatterns = [
    path("hats/", api_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_show_hat, name="api_show_hat"),
    # path("locations/<int:pk>/", api_location, name="api_location"),
    # path("bins/", api_bins, name="api_bins"),
    # path("bins/<int:pk>/", api_bin, name="api_bin"),
]
