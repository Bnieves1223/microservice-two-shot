
from django.http import JsonResponse
from django.shortcuts import render
from .models import BinVO, Shoe
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder

# Create your views here.

class BinVoEncoder(ModelEncoder):
    model = BinVO
    properties =[
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href"
    ]

class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties =[
        "id",
        "manufacturer",
        "brand",
        "color",
        "picture",
        "bin",
    ]
    encoders = {"bin": BinVoEncoder()}
    
    
@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            href = content["bin"]
            bin = BinVO.objects.get(import_href=href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )
        
@require_http_methods(["DELETE"])
def api_shoe(request, pk):
    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
            
                