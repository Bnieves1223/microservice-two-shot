from time import clock_settime
from django.db import models


# Create your models here.

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField(null=True,blank=True)
    bin_size = models.PositiveSmallIntegerField(null=True,blank=True)
    import_href = models.CharField(max_length=200,unique=True,null=True,blank=True)
    

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    brand = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture = models.URLField()
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True,
    )
