import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'))


async function loadWardrobe() {
  let shoedata;
  let hatdata;

  const hatresponse = await fetch('http://localhost:8090/api/hats/');
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/')

  if (hatresponse.ok) {
    hatdata = await hatresponse.json();
  } else {
    console.error('response is not ok')
  }
  if (shoeResponse.ok) {
    shoedata = await shoeResponse.json();
  } else {
    console.error('response is not ok')
  }


  root.render(
    <React.StrictMode>
      <App hats={hatdata} shoes={shoedata}/>
    </React.StrictMode>
  );


}
loadWardrobe();
