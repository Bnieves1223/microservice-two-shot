import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import HatsTable from './HatsTable';
import HatsDelete from './HatsDelete';
import ShoesList from './Shoeslist';

function App(props) {
  if (props.hats == undefined) {
    return null
  }

// function App(props){
//   if  (props.shoes === undefined) {
//     return null;
//   }
// }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats">
            <Route path="" element={<HatsList hats={props.hats}/>}/>
            <Route path="new" element={<HatsForm/>}/>
            <Route path="table" element={<HatsTable hats={props.hats}/>}/>
            <Route path="delete" element={<HatsDelete hats={props.hats}/>}/>
          </Route>
          <Route path="/shoes" element={<ShoesList arr={props.shoes} />} />
        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
