import React from 'react';
import { Link } from 'react-router-dom'

function HatsTable(props) {

    console.log(props.hats)

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>Hat Id</th>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.id }</td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.closet_name }</td>
                            <td><Link to="/hats/delete">Delete</Link></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatsTable;
