import React from 'react';

class HatsDelete extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            hats: [],
        };
        this.handleIdChange = this.handleIdChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.hats;
        console.log(data);

        const hatUrl = `http://localhost:8090/api/hats/${data.id}/`;
        console.log(data)
        const fetchConfig = {
          method: "DELETE",
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
          const newHat = await response.json();
          console.log(newHat)
          const cleared = {
            id: '',
        }
        this.setState(cleared);
    }
    }

    handleIdChange(event) {
        const value = event.target.value;
        this.setState({id: value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/api/hats/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();

          this.setState({hats: data.hats});
        }
      }


    render() {
        return (
<div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Choose hat to delete</h1>
            <form onSubmit={this.handleSubmit} id="delete-hat">
              <div className="mb-3">
                <select onChange={this.handleIdChange} required id="id"
                        className="form-select" name="id" value={this.state.id}>
                    <option value="">Choose hat to delete</option>
                        {this.state.hats.map(hat => {
                            return (
                                <option key={hat.href} value={hat.href}>
                                    {hat.id}
                                </option>
                                );
                        })}
                </select>
                </div>
              <button className="btn btn-primary">Delete</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}

export default HatsDelete;
