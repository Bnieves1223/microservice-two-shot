import React from 'react'
import { Link } from 'react-router-dom'

async function onDelete(id) {
    const url = `http://localhost:8090/api/hats/${id}/`
    const fetchConfig = {
        method: "DELETE",
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
        console.log(`Shoe Deleted`, response);
    } else {
        console.error(response);
    }
}

class HatsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hatColumns: [[], [], []],
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats/'
    try {
      const response = await fetch(url)
      if (response.ok) {
        const data = await response.json()
        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090/api/hats/${hat.id}/`
          requests.push(fetch(detailUrl))
        }
        const responses = await Promise.all(requests)
        const hatColumns = [[], [], []];
        let i = 0
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json()
            hatColumns[i].push(details)
            i = i + 1
            if (i > 2) {
              i = 0
            }
          } else {
            console.error(hatResponse)
          }
        }
        this.setState({hatColumns: hatColumns})
      }
    } catch (e) {
      console.error(e)
    }
  }
  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
          <h1 className="display-5 fw-bold">Hats List!</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
                Welcome to your hat wardrobe.
            </p>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add new hat</Link>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            {this.state.hatColumns.map((list, key) => {
              return (
                <div className="col" key={key}>
                {list.map(hat => {
                  return (
                    <div key={hat.id} className="card mb-3 shadow">
                      <img src={hat.picture_url} className="card-img-top" />
                      <div className="card-body">
                        <h5 className="card-text">{hat.fabric}</h5>
                        <h5 className="card-text">{hat.style_name}</h5>
                        <h5 className="card-text">{hat.color}</h5>
                      </div>
                      <div className="card-footer">
                          <h6>{hat.closet_name}</h6>
                          <h6>Section {hat.section_number}</h6>
                          <h6>Shelf {hat.shelf_number}</h6>
                      </div>
                      <button type="button" className="btn btn-outline-danger" onClick={() => onDelete(hat.id)}>Delete</button>
                    </div>
                  )
                })}
                </div>
              )
            })}
          </div>
        </div>
      </>
    );
  }
}

export default HatsList
