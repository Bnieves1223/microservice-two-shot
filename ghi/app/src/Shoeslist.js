function ShoesList(props) {
    return (
      <table className="table table-striped">
        <thead>
          <tr className ='table-primary'>
            <th>Brand</th>
            <th>Color</th>
            <th>Image</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {console.log(props.arr)}
          {props.arr.shoes.map(shoe => {
            return (

              <tr key={shoe.bin.import_href}>
                <td>{shoe.brand}</td>
                <td>{shoe.color}</td>
                <td>{shoe.picture}</td>
                <td>{shoe.manufacturer}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoesList;
