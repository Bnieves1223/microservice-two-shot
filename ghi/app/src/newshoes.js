import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            manufacturer: '',
            brand: '',
            color: '',
            picture: '',
            bin: '',
            bins: [],
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
        this.handleChangeBrand = this.handleChangeBrand.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangePicture = this.handleChangePicture.bind(this);
        this.handleChangeBin = this.handleChangeBin.bind(this);
        
}

async componentDidMount() {
    const url  = "h	http://localhost:8100/api/bins/"
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations });
    }
  }


async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.bin
    delete data.pictures
    const url  = "http://localhost:8080/api/shoes/"
    const fetchConfig ={
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok){
        const newConference= await response.json();
        console.log(newConference);
        this.setState({
            manufacturer: '',
            brand: '',
            color: '',
            picture: '',
            bin: '',
        });
    }
}

handleChangeManufacturer(event){
    const value = event.target.value;
    this.setState({ manufacturer: value });
}

handleChangeBrand(event){
    const value = event.target.value;
    this.setState({ brand: value });
}

handleChangeColor(event){
    const value = event.target.value;
    this.setState({ color: value });
}

handleChangePicture(event){
    const value = event.target.value;
    this.setState({ picture: value });
}

handleChangeBin(event){ 
    const value = event.target.value;
    this.setState({ bin: value });





























render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={this.handleSubmit} id="create-shoes-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeManufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" placeholder="className="form-control" />
                <label htmlFor="name">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeBrand} placeholder="brand" required type="text" name="brand" id="brand" className="form-control" />
                <label htmlFor="starts">Brand</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeColor} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="ends">Color</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Picture</label>
                <textarea onChange={this.handleChangePicture} className="form-control" id="picture" rows="3" name="picture" className="form-control"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeBin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.id}>{bin.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoeForm;




